# -*- coding: utf-8 -*-
"""
Application configuration
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

from PySide.QtCore import QLocale
import os

application = dict()
database = dict()

# Debug
application['commandLineDebug'] = True
application['logDebug'] = False

# Store
application['store'] = 'Toko Jaya Abadi'

# Version
application['major'] = '0'
application['minor'] = '1'
application['release'] = '0'
application['version'] = application['major'] + '.' + application['minor'] + '.' + application['release']

# Language
application['language'] = QLocale.Indonesian

# Country where you reside
application['country'] = QLocale.Indonesia

#appLocale = QLocale(language, country)
application['locale'] = QLocale.system()
application['locale'].setNumberOptions(QLocale.OmitGroupSeparator)

# Table View Limit
application['table_paging_limit'] = 3


# Database
database['driver'] = 'QSQLITE' #QODBC #QPSQL #QDB2 # See QSqlDatabase manual
database['hostname'] = 'localhost'
database['username'] = ''
database['password'] = ''
database['database'] = 'database/store.db'
database['options'] = ''

# MYSQL specific options
database['QMYSQL'] = dict() # Uncomment if used
# MySQL terkadang ditangani oleh akonadi yang dijalankan per-pengguna(non-root),
# kosongkan bila menggunakan socket default, yaitu; /var/run/mysql/socket-mysql
socket = os.environ['HOME'] + "/.local/share/akonadi/socket-" + os.environ['HOSTNAME'] + "/mysql.socket"
if os.path.exists(socket):
    database['QMYSQL']['UNIX_SOCKET'] = socket
	
#database['QMYSQL']['CLIENT_SSL'] = 
#database['QMYSQL']['CLIENT_ODBC'] = 

# SQLITE specific options
database['QSQLITE'] = dict()

# Compiling driver options
_separator = ';'
_assignment = '='
_tempList = list()
_iterator = database[database['driver']].iteritems()
for i in database[database['driver']]:
	_tempList.append(_assignment.join(_iterator.next()))
database['options'] = _separator.join(_tempList)
