
Bismillahirrahmanirrahim

Alhamdulillahi rabbil 'alamin.
Allahumma shalli 'ala Muhammad wa 'ala ali Muhammad.

---------------------------------------------------------------------------------
2012/12/25
----------
1. Rancangan halaman yang akan ditampilkan
   Home (Kilasan)
    |
    +-Pembelian
    |   |
    |   +-Supplier
    |   |
    |   +-Nota   
    |
    +-Penyimpanan
    |   |
    |   +-Gudang
    |   |
    |   +-Rak
    |
    +-Barang
        |
        +-Kategori
        |
        +-Barang
    <widget class="QStackedWidget" name="mainStackedWidget">
    <property name="geometry">
     <rect>
      <x>12</x>
      <y>12</y>
      <width>1000</width>
      <height>530</height>
     </rect>
    </property>
    <widget class="QWidget" name="pageData">
     <widget class="QWidget" name="gridLayoutWidget">
      <property name="geometry">
       <rect>
        <x>-1</x>
        <y>-1</y>
        <width>998</width>   <<---- use these dimension for new stacked widgets
        <height>528</height>
       </rect>
      </property
     </widget>
    </widget>




2012/09/17
----------
Hal yang saya pelajari dalam membuat basisdata dalam proyek ini adalah:
1. Tabel dibuat berdasarkan data mentah di lapangan, berdasarkan entitas-entitas
   yang telah berjalan. Sebisa mungkin menunda untuk mengambil hal-hal abstrak
   sebagai sebuah entitas tabel karena kebanyakan hal-hal abstrak umumnya juga
   hadir sebagai penghubung antara hal-hal konkret yang terjadi.
   Seperti tabel "harga jual" yang akan menjadi lebih relevan  setelah kita 
   membuat tabel "stok", dan tabel "stok" sesudah tabel "nota belanja", begitu
   seterusnya.
   
2. Tabel-tabel bantuan akan diciptakan untuk membantu relasi antar tabel-tabel
   dasar tersebut.




2012/09/11
----------
Beberapa proposal tampilan aplikasi
1. Tampilan config-dialog
   Mirip seperti tabbed window, hanya saja tab-nya berada disamping kiri, bukan di
   atas.
   Widget utama/dokumennya ada di samping kanan. Bentuk antarmuka untuk bagian
   kanan ini bisa menyusul.

2. Splitter 
   Splitter mirip seperti editor bookmark di browser-browser pada umumnya.
   Terdiri dari tiga bagian, bagian kiri yang menampilan hari, kemudian
   bagian kanan atas menampilkan daftar(list) bookmark yang ada.
   Dan bagian ketiga adalah bagian detail(edit mode) dari bookmark yang
   telah dipilih di jendela ke dua.

View yang layak untuk dijadikan fokus utama
(Untuk memudahkan gambaran kita akan meminjam antarmuka splitter)
1. Berdasarkan Kategori Jenis Barang

2. Berdasarkan Toko Penjual

3. Berdasarkan Rak Penyimpanan



2012/08/17
----------
Aturan tambahan:
1. Pengkategorian barang harus mengikuti azas spesifik ke umum, artinya
   sebisa mungkin sebuah barang dimasukkan ke kategori spesifiknya sebelum 
   dimasukkan ke dalam kategori umumnya(bila kategori spesifiknya belum ada).

   Misal
   +---------------+   +---- +------------------+--------+
   | Produk        |   | id  | Kategori         | Parent |
   +---------------+   +---- +------------------+--------+
   | Lampu Belajar |\  | 1   | Elektronik       | NULL   |
   |               | \ | 2   | Penerangan       | 1      |
   +---------------+   +-----+------------------+--------+

    Lampu Belajar -----> Penerangan (Tepat)

    Lampu Belajar --///--> Elektronik (Keliru)


2. Table __produk_detail__ yang berisi kolom-kolom atribut/properti yang
   berpotensi menjadi sangat tidak efisien, seperti panjang, lebar,
   kadaluarsa, tebal, dan lain-lain.
   Oleh karena itu maka atribut-atribut tersebut sebaiknya dimasukkan 
   menjadi sebuah kode yang kompak, artinya kita butuh mekanisme yang bisa
   menjejalkan atribut-atribut tersebut menjadi sebuah nilai yang nantinya
   akan diparse dgn aturan tertentu :)

   atribut parser
   --------------------
   # importing regex
   import re

   def parse(value, group=0):
       return re.search('(\d+)([a-z]+)',value).group(group)

   # v = '250ml'
   parse(v)
   250ml
   parse(v,1)
   250
   parse(v,2)
   ml
   
   def getProp(prop, key):
...     word = re.search('(\d+)([a-z]+)',prop[key])
...     return (key, word.group(1), word.group(2))
        # mengembalikan tuple
        
   balok = {'Lebar': '3m', 'Panjang': '25m', 'Tinggi': '2.5m'}

   >>> for k in balok.keys():
   ...     print getProp(balok, k)
   ... 
   ('Lebar', '3', 'm')
   ('Panjang', '25', 'm')
  ('Tinggi', '5', 'm')

   --------------------

   atribut-atribut yang akan digunakan *(sewaktu-waktu bisa mengalami revisi)
   --------------------------------------------------------------------------
   a. Dalam bentuk python dictionary
      Property = {'Panjang':'25m', 'Lebar':'3m', 'Tinggi':'5m'}
   
   b. Tipe/jenis dan key-key yang digunakan, antara lain:
      - Balok:
        Panjang
        Lebar
        Tinggi
        
      - Tubular(pipa, selang):
        Panjang
        Diameter
      
      - Visual:
        Warna
      
      - Buku/kertas:
        Tebal
        
      - Isi:
        Volume
        
      - Masa:
        Berat
      
      - Tambahan:
        Model
        Varian

3. Ketika menyimpan stok dari produk kita perlu membuat kolom-kolom yang terkait,
   seperti 'jumlah stok', dan lainnya, seperti:
   a. Kondisi:
      - baru
      - bekas
      - rusak
   b. Grade
      - A, B, C, D, E; Istimewa, Baik, Cukup, Kurang, Buruk
   c. Mfg:
      - kadaluarsa
      - mfg
   d. Jumlah









