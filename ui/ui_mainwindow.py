# -*- coding: utf-8 -*-
"""
UI Main Window
Central window of the application
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

from PySide import QtCore, QtGui
from ui.widget_central import centralWidget
from ui.ui_configuration import configurationWidget
from ui.ui_product import productWidget
from ui.ui_dealer import dealerWidget
from lib import logger


class ui_mainWindow(object):

    def __init__(self, database):
        """
            Inisiasi menu; menubar, toolbar beserta action dan
            submenu-submenunya.
        """
        
        self.database = database        

        self.mainWindow = QtGui.QMainWindow()
        self.mainWindow.setObjectName("MainWindow")
        self.mainWindow.resize(1024, 600)

        self.log = logger.log(self.mainWindow)

        # Widget-widget yang akan kita gunakan
        self.stackedWidgets = dict()
        self.widgetStacker = QtGui.QStackedWidget(self.mainWindow)
        self.initMainWidgets()
        self.setMainWidget('product')

        # Menu Bar
        self.menubar = QtGui.QMenuBar(self.mainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 640, 20))
        self.menubar.setObjectName("menubar")

        # Tool Bar
        self.toolBar = QtGui.QToolBar(self.mainWindow)
        self.toolBar.setObjectName("toolBar")
        self.mainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)

        # Menus
        self.menuPerkakas = QtGui.QMenu(self.menubar)
        self.menuPerkakas.setObjectName("menuPerkakas")
        
        self.menuStatistik = QtGui.QMenu(self.menubar)
        self.menuStatistik.setObjectName("menuStatistik")

        self.menuPembelian = QtGui.QMenu(self.menubar)
        self.menuPembelian.setObjectName("menuPembelian")

        self.menuPenyimpanan = QtGui.QMenu(self.menubar)
        self.menuPenyimpanan.setObjectName("menuPenyimpanan")

        self.menuBarang = QtGui.QMenu(self.menubar)
        self.menuBarang.setObjectName("menuBarang")

        self.menuPenjualan = QtGui.QMenu(self.menubar)
        self.menuPenjualan.setObjectName("menuPenjualan")
        
        self.menuBantuan = QtGui.QMenu(self.menubar)
        self.menuBantuan.setObjectName("menuBantuan")

        # Status Bar
        self.statusbar = QtGui.QStatusBar(self.mainWindow)
        self.statusbar.setObjectName("statusbar")

        # Actions
        # - Menu Perkakas
        self.actionJejak = QtGui.QAction(self.mainWindow)
        self.actionJejak.setObjectName("actionJejak")
        
        self.actionPraCetak = QtGui.QAction(self.mainWindow)
        self.actionPraCetak.setObjectName("actionPraCetak")
        
        self.actionCetak = QtGui.QAction(self.mainWindow)
        self.actionCetak.setObjectName("actionCetak")

        self.actionLaporan = QtGui.QAction(self.mainWindow)
        self.actionLaporan.setObjectName("actionLaporan")
        
        self.actionKonfigurasi = QtGui.QAction(self.mainWindow)
        #self.actionKonfigurasi.setCheckable(True)
        self.actionKonfigurasi.setObjectName("actionKonfigurasi")
        
        self.actionTutup = QtGui.QAction(self.mainWindow)
        self.actionTutup.setObjectName("actionTutup")
        
        # - Menu Statistik
        self.actionKilasan = QtGui.QAction(self.mainWindow)
        self.actionKilasan.setObjectName("actionKilasan")

        self.actionStatistik = QtGui.QAction(self.mainWindow)
        self.actionStatistik.setObjectName("actionStatistik")

        # - Menu Pembelian
        self.actionSupplier = QtGui.QAction(self.mainWindow)
        self.actionSupplier.setObjectName("actionSupplier")

        self.actionNotaMasuk = QtGui.QAction(self.mainWindow)
        self.actionNotaMasuk.setObjectName("actionNotaMasuk")

        self.actionFakturMasuk = QtGui.QAction(self.mainWindow)
        self.actionFakturMasuk.setObjectName("actionFakturMasuk")

        # - Menu Penyimpanan
        self.actionGudang = QtGui.QAction(self.mainWindow)
        self.actionGudang.setObjectName("actionGudang")

        self.actionRak = QtGui.QAction(self.mainWindow)
        self.actionRak.setObjectName("actionRak")

        # - Menu Barang
        self.actionKategori = QtGui.QAction(self.mainWindow)
        self.actionKategori.setObjectName("actionKategori")
        
        self.actionProduk = QtGui.QAction(self.mainWindow)
        self.actionProduk.setObjectName("actionProduk")

        # - Menu Penjualan
        self.actionNotaKeluar = QtGui.QAction(self.mainWindow)
        self.actionNotaKeluar.setObjectName("actionNotaKeluar")

        self.actionFakturKeluar = QtGui.QAction(self.mainWindow)
        self.actionFakturKeluar.setObjectName("actionFakturKeluar")
        
        # - Menu Bantuan
        self.actionTentang = QtGui.QAction(self.mainWindow)
        self.actionTentang.setObjectName("actionTentang")
        
        self.actionTentangQt = QtGui.QAction(self.mainWindow)
        self.actionTentangQt.setObjectName("actionTentantQt")

        # Adding actions into menus
        # - Menu Perkakas
        self.menuPerkakas.addAction(self.actionJejak)
        self.menuPerkakas.addSeparator()
        self.menuPerkakas.addAction(self.actionPraCetak)
        self.menuPerkakas.addAction(self.actionCetak)
        self.menuPerkakas.addSeparator()        
        self.menuPerkakas.addAction(self.actionLaporan)
        self.menuPerkakas.addSeparator()
        self.menuPerkakas.addAction(self.actionKonfigurasi)
        self.menuPerkakas.addSeparator()
        self.menuPerkakas.addAction(self.actionTutup)

        # - Menu Statistik
        self.menuStatistik.addAction(self.actionKilasan)
        self.menuStatistik.addAction(self.actionStatistik)

        # - Menu Pembelian
        self.menuPembelian.addAction(self.actionSupplier)        
        self.menuPembelian.addAction(self.actionNotaMasuk)
        self.menuPembelian.addAction(self.actionFakturMasuk)

        # - Menu Penyimpanan
        self.menuPenyimpanan.addAction(self.actionGudang)
        self.menuPenyimpanan.addAction(self.actionRak)

        # - Menu Barang
        self.menuBarang.addAction(self.actionKategori)
        self.menuBarang.addAction(self.actionProduk)

        # - Menu Penjualan
        self.menuPenjualan.addAction(self.actionNotaKeluar)
        self.menuPenjualan.addAction(self.actionFakturKeluar)

        # - Menu Bantuan
        self.menuBantuan.addAction(self.actionTentang)
        self.menuBantuan.addAction(self.actionTentangQt)

        # Adding menus into Menu Bar
        self.menubar.addAction(self.menuPerkakas.menuAction())
        self.menubar.addAction(self.menuStatistik.menuAction())
        self.menubar.addAction(self.menuPembelian.menuAction())
        self.menubar.addAction(self.menuPenyimpanan.menuAction())
        self.menubar.addAction(self.menuBarang.menuAction())
        self.menubar.addAction(self.menuPenjualan.menuAction())
        self.menubar.addAction(self.menuBantuan.menuAction())

        # Adding actions into Tool Bar
        self.toolBar.addAction(self.actionJejak)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionPraCetak)
        self.toolBar.addAction(self.actionCetak)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionSupplier)
        self.toolBar.addAction(self.actionKategori)
        self.toolBar.addAction(self.actionProduk)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionKonfigurasi)

        # Adding Menu Bar, Tool Bar, Status Bar and Central Widget into mainWindow
        self.mainWindow.setMenuBar(self.menubar)
        self.mainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.mainWindow.setStatusBar(self.statusbar)
        self.mainWindow.setCentralWidget(self.widgetStacker)

        # Koneksi-koneksi menu pada action
        # - Koneksi Perkakas
        #self.actionJejak
        #self.actionJejak
        
        #self.actionPraCetak
        #self.actionPraCetak
        
        #self.actionCetak
        #self.actionCetak

        #self.actionLaporan
        #self.actionLaporan
        
        # - Koneksi Konfigurasi
        self.actionKonfigurasi.setStatusTip("Atur konfigurasi")
        #self.actionKonfigurasi.setCheckable(True)
        self.actionKonfigurasi.triggered.connect(self.showConfiguration)

        # - Koneksi Keluar
        self.actionTutup.setStatusTip("Keluar dari aplikasi")
        self.actionTutup.triggered.connect(self.mainWindow.close)

        # - Koneksi Statistik
        #self.actionKilasan
        #self.actionKilasan

        #self.actionStatistik
        #self.actionStatistik
        
        # - Koneksi Pembelian
        self.actionSupplier.setStatusTip("Tampilkan data supplier")
        self.actionSupplier.triggered.connect(self.showSupplier)
        #self.actionNotaMasuk
        #self.actionNotaMasuk
        #self.actionFakturMasuk
        #self.actionFakturMasuk

        # - Koneksi Penyimpanan
        #self.actionGudang
        #self.actionGudang
        #self.actionRak
        #self.actionRak

        # - Koneksi Barang
        self.actionProduk.setStatusTip("Tampilkan data produk")
        self.actionProduk.triggered.connect(self.showProduct)
        #self.actionKategori
        #self.actionKategori
        
        # - Koneksi Penjualan
        #self.actionNotaKeluar
        #self.actionNotaKeluar
        #self.actionFakturKeluar
        #self.actionFakturKeluar
        
        # - Koneksi Bantuan
        self.actionTentang.setStatusTip("Tentang aplikasi ini")
        self.actionTentang.triggered.connect(self.log.aboutApp)
        self.actionTentangQt.setStatusTip("Tentang Qt")
        self.actionTentangQt.triggered.connect(QtGui.qApp.aboutQt)

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
        
        
    def setMainWidget(self, widgetName):
        self.widgetStacker.setCurrentWidget(self.stackedWidgets[widgetName])


    def initMainWidgets(self):
        # Tampilkan produk sebagai halaman pertama.
        # Silahkan diganti sesuai halaman keinginan Anda.
        self.showProduct()


    def showConfiguration(self):
        if self.stackedWidgets.has_key('configuration') == False:
            self.stackedWidgets['configuration'] = configurationWidget(self.database, self.mainWindow)
            self.stackedWidgets['configuration'].setObjectName('configurationWidget')
            self.widgetStacker.addWidget(self.stackedWidgets['configuration'])
        self.setMainWidget('configuration')


    def showProduct(self):
        if self.stackedWidgets.has_key('product') == False:
            self.stackedWidgets['product'] = productWidget(self.database, self.mainWindow)
            self.stackedWidgets['product'].setObjectName("productWidget")
            self.widgetStacker.addWidget(self.stackedWidgets['product'])
        self.setMainWidget('product')


    def showSupplier(self):
        if self.stackedWidgets.has_key('dealer') == False:
            self.stackedWidgets['dealer'] = dealerWidget(self.database, self.mainWindow)
            self.stackedWidgets['dealer'].setObjectName("dealerWidget")
            self.widgetStacker.addWidget(self.stackedWidgets['dealer'])
        self.setMainWidget('dealer')


    def retranslateUi(self):
        self.mainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.menuPerkakas.setTitle(QtGui.QApplication.translate("MainWindow", "&Perkakas", None, QtGui.QApplication.UnicodeUTF8))
        self.menuPembelian.setTitle(QtGui.QApplication.translate("MainWindow", "&Pembelian", None, QtGui.QApplication.UnicodeUTF8))
        self.menuPenyimpanan.setTitle(QtGui.QApplication.translate("MainWindow", "&Penyimpanan", None, QtGui.QApplication.UnicodeUTF8))
        self.menuBarang.setTitle(QtGui.QApplication.translate("MainWindow", "&Barang", None, QtGui.QApplication.UnicodeUTF8))
        self.menuPenjualan.setTitle(QtGui.QApplication.translate("MainWindow", "&Penjualan", None, QtGui.QApplication.UnicodeUTF8))
        self.menuStatistik.setTitle(QtGui.QApplication.translate("MainWindow", "&Statistik", None, QtGui.QApplication.UnicodeUTF8))
        self.menuBantuan.setTitle(QtGui.QApplication.translate("MainWindow", "Bant&uan", None, QtGui.QApplication.UnicodeUTF8))
        self.toolBar.setWindowTitle(QtGui.QApplication.translate("MainWindow", "toolBar", None, QtGui.QApplication.UnicodeUTF8))
        self.actionJejak.setText(QtGui.QApplication.translate("MainWindow", "&Jejak", None, QtGui.QApplication.UnicodeUTF8))
        self.actionPraCetak.setText(QtGui.QApplication.translate("MainWindow", "P&ra Cetak", None, QtGui.QApplication.UnicodeUTF8))
        self.actionCetak.setText(QtGui.QApplication.translate("MainWindow", "&Cetak", None, QtGui.QApplication.UnicodeUTF8))
        self.actionLaporan.setText(QtGui.QApplication.translate("self.mainWindow", "&Laporan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionKonfigurasi.setText(QtGui.QApplication.translate("MainWindow", "&Konfigurasi", None, QtGui.QApplication.UnicodeUTF8))
        self.actionTutup.setText(QtGui.QApplication.translate("MainWindow", "&Tutup", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSupplier.setText(QtGui.QApplication.translate("MainWindow", "&Supplier", None, QtGui.QApplication.UnicodeUTF8))
        self.actionKategori.setText(QtGui.QApplication.translate("MainWindow", "&Kategori", None, QtGui.QApplication.UnicodeUTF8))
        self.actionProduk.setText(QtGui.QApplication.translate("MainWindow", "&Produk", None, QtGui.QApplication.UnicodeUTF8))
        self.actionNotaMasuk.setText(QtGui.QApplication.translate("MainWindow", "&Nota Masuk", None, QtGui.QApplication.UnicodeUTF8))
        self.actionFakturMasuk.setText(QtGui.QApplication.translate("MainWindow", "&Faktur Masuk", None, QtGui.QApplication.UnicodeUTF8))
        self.actionNotaKeluar.setText(QtGui.QApplication.translate("MainWindow", "&Nota Keluar", None, QtGui.QApplication.UnicodeUTF8))
        self.actionFakturKeluar.setText(QtGui.QApplication.translate("MainWindow", "&Faktur Keluar", None, QtGui.QApplication.UnicodeUTF8))
        self.actionGudang.setText(QtGui.QApplication.translate("MainWindow", "&Gudang", None, QtGui.QApplication.UnicodeUTF8))
        self.actionRak.setText(QtGui.QApplication.translate("MainWindow", "&Rak", None, QtGui.QApplication.UnicodeUTF8))
        self.actionStatistik.setText(QtGui.QApplication.translate("MainWindow", "S&tatistik", None, QtGui.QApplication.UnicodeUTF8))
        self.actionKilasan.setText(QtGui.QApplication.translate("MainWindow", "&Kilasan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionTentang.setText(QtGui.QApplication.translate("MainWindow", "&Tentang", None, QtGui.QApplication.UnicodeUTF8))
        self.actionTentangQt.setText(QtGui.QApplication.translate("MainWindow", "Tentang &Qt", None, QtGui.QApplication.UnicodeUTF8))


    def runUi(self):
        self.mainWindow.show()
        #self.setMaximumSize(QtGui.QApplication.desktop().screenGeometry().size())


"""
TODO:
1. It is advisable to convert all these tabbings into SDI or MDI, since they were designed to preserve
   the chronological windows opened.

"""   
